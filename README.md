# SVGO - Local environment utility

## Installation

Run `yarn install` in your project folder terminal

## Get started

1. Copy your SVG files in the svg-source folder
2. Run `yarn svg` in your project folder terminal
3. Copy / Use the newly created SVG files from the svg-result folder

## Utilities

#### Delete all SVG

Run `yarn clean` in your project folder to delete all SVG files in svg-source and svg-result folders.

## Config

Default options :

- Plugin : `preset-default`
- Overridden : `removeViewBox` -> `false`

You can change the config in the `svgo.config.js` file with any option from [SVGO tool](https://github.com/svg/svgo).